export const sliderItems = [
    {
        id: 1,
        img: 'https://images.pexels.com/photos/1263986/pexels-photo-1263986.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
        title: 'SUMMER SALE',
        desc: 'Go grab your makeup kits now and get 30% discount in every products that you will purchase.',
        bg: '#FBDAF9',
    },
    {
        id: 2,
        img: 'https://images.pexels.com/photos/293029/pexels-photo-293029.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
        title: 'WINTER SALE',
        desc: 'Go grab your skincare products now and get 30% discount in every products that you will purchase.',
        bg: '#FBDAF9',
    },
    {
        id: 3,
        img: 'https://images.pexels.com/photos/8467480/pexels-photo-8467480.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
        title: 'FEATURED',
        desc: 'Go grab your products now and get 30% discount in every products that you will purchase.',
        bg: '#FBDAF9'
    }
]

export const categories = [
    {
        id: 1,
        img: 'https://images.pexels.com/photos/3018845/pexels-photo-3018845.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
        title: 'COSMETICS',

    },
    {
        id: 2,
        img: 'https://images.pexels.com/photos/4672654/pexels-photo-4672654.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260',
        title: 'SKIN CARE PRODUCTS',

    },
    {
        id: 3,
        img: 'https://images.pexels.com/photos/3018845/pexels-photo-3018845.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
        title: 'HAIR PRODUCTS'

    }
]

export const featuredProducts = [
    {
        id: 1,
        img: '../images/blushon.jpg',

    },
    {
        id: 2,
        img: '../images/cccushion.jpg',

    },
    {
        id: 3,
        img: '../images/hairmask.jpg',

    }
]