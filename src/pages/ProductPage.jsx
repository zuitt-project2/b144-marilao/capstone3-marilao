import { Add, Remove } from "@material-ui/icons";
import styled from "styled-components"
import Announcements from "../components/Announcements";
import AppNavbar from "../components/AppNavbar";
import Footer from "../components/Footer";
import { mobile } from "../responsive";

const Container = styled.div`
    background-color: #F4C1FA;
`;
const Wrapper = styled.div`
    padding: 50px;
    display: flex;
    ${mobile({ flexDirection: 'column', padding: '10px' })}
`;
const ImgContainer = styled.div`
    flex:1;
`;
const Image = styled.img`
    width: 100%;
    height: 90vh;
    object-fit: cover;
    ${mobile({ height: '40vh' })}
`;
const InfoContainer = styled.div`
    flex:1;
    padding:0px 50px;
    ${mobile({ padding: '10px' })}
`;
const Title = styled.h1`
    font-weight: 200;
`;
const Desc = styled.p`
    margin: 20px 0px;
`;
const Price = styled.span`
    font-weight: 100;
    font-size: 40px;
`;

const FilterContainer = styled.div`
    width: 50%;
    justify-content: space-between;
    display: flex;
    margin: 30px 0px;
    ${mobile({ width: '100%' })}
`;
const Filter = styled.div`
    display: flex;
    align-items: center;
`;
const FilterColor = styled.div`
    width: 20px;
    height: 20px;
    border-radius: 50%;
    background-color: ${props => props.color};
    margin: 0px 5px;
    cursor: pointer;
`;
const FilterTitle = styled.span`
    font-size: 20px;
    font-weight: 200;
`;

const AddContainer = styled.div`
    display: flex;
    align-items: center;
    width: 50%;
    justify-content: space-between;
    ${mobile({ width: '100%' })}
`;
const AmountContainer = styled.div`
    display: flex;
    align-items: center;
    font-weight: 700;

`;
const Amount = styled.span`
    width: 30px;
    height: 30px;
    border-radius: 10px;
    border: 1px solid black;
    display: flex;
    align-items: center;
    justify-content: center;
    margin: 0px 5px;
`;
const Button = styled.button`
    display: flex;
    padding: 15px;
    border: 1px solid black;
    background-color: white;
    cursor: pointer;
    border-radius: 30%;
    font-weight: 500;

    &:hover {
        background-color: pink;
    }
`;

const ProductPage = () => {
    return (
        <Container>
            <AppNavbar />
            <Announcements />
            <Wrapper>
                <ImgContainer>
                    <Image src='../images/1.jpg' />
                </ImgContainer>
                <InfoContainer>
                    <Title>MINI MICROCURRENT TONING FACE LIFT WRINKLE REMOVAL LYMPHATIC MASSAGER</Title>
                    <Desc>Microcurrent technology is one of the hottest innovations in the anti-aging industry and is widely used in top spas and medical offices for facial toning, tightening, and firming of aging skin. A low-level current that typically operates in the range of 0-400 microamps, which is a safe and effective technology for clients who wish to attainand maintain a healthier, younger looking appearance. The results can be so dramatic that microcurrent treatments are considered as “non-surgical face lifts.” </Desc>
                    <Price> ₱ 5,000</Price>
                    <FilterContainer>
                        <Filter>
                            <FilterTitle>Color:</FilterTitle>
                            <FilterColor color='white' />
                            <FilterColor color='pink' />
                        </Filter>
                    </FilterContainer>
                    <AddContainer>
                        <AmountContainer>
                            <Remove />
                            <Amount>1</Amount>
                            <Add />
                        </AmountContainer>
                        <Button>Add to Cart</Button>
                    </AddContainer>
                </InfoContainer>
            </Wrapper>
            <Footer />
        </Container>
    )
}

export default ProductPage
