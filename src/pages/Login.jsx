import styled from 'styled-components';
import { mobile } from '../responsive';

const Container = styled.div`
    width: 100vw;
    height: 100vh;
    background:linear-gradient(rgba(255,255,255,.5),rgba(255,255,255,.5)),
     url('https://images.pexels.com/photos/4041391/pexels-photo-4041391.jpeg?auto=compress&cs=tinysrgb&h=650&w=940')
     center;
    background-size: cover;
     display: flex;
     align-items: center;
     justify-content: center;

`;
const Wrapper = styled.div`
    padding: 20px;
    width: 25%;
    background-color: white;
    ${mobile({ width: '75%' })}
`;
const Form = styled.form`
    display: flex;
    flex-direction: column;
`;
const Title = styled.h1`
    font-size: 24px;
    font-weight: bold;
    text-align: center;
`;
const Input = styled.input`
    flex:1;
    min-width: 40%;
    margin: 10px 0px;
    padding: 10px;
`;
const Button = styled.button`
    width: 40%;
    border: none;
    padding: 15px 20px;
    background-color: pink;
    font-color: white;
    cursor: pointer;
    margin-bottom: 10px;
    margin: auto;
`;
const Link = styled.a`
    margin: 5px 0px;
    margin: auto;
    font-size: 12px;
    text-decoration: underline;
    cursor: pointer;
`;
const Login = () => {

    return (
        
            <Container>
                <Wrapper>
                    <Title>LOGIN</Title>
                    <Form >
                        <Input placeholder='email' />
                        <Input placeholder='password' />
                        <Button>LOGIN</Button>
                        <Link>CREATE A NEW ACCOUNT</Link>
                    </Form>
                </Wrapper>
            </Container>
    )
}

export default Login;