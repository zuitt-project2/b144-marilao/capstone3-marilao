
import styled from 'styled-components';
import { mobile } from '../responsive';



const Container = styled.div`
    width: 100vw;
    height: 100vh;
    background:linear-gradient(rgba(255,255,255,.5),rgba(255,255,255,.5)),
     url('https://images.pexels.com/photos/4041392/pexels-photo-4041392.jpeg?auto=compress&cs=tinysrgb&h=650&w=940')
     center;
     background-size: cover;
     display: flex;
     align-items: center;
     justify-content: center;

`;
const Wrapper = styled.div`
    padding: 20px;
    width: 40%;
    background-color: white;
    ${mobile({ width : '75%'})}
`;
const Form = styled.form`
    display: flex;
    // flex-wrap: wrap;
    flex-direction: column;
`;
const Title = styled.h1`
    font-size: 24px;
    font-weight: bold;
    text-align:center;
`;
const Input = styled.input`
    flex:1;
    min-width: 40%;
    margin: 10px 5px 5px;
    padding: 10px;
`;
const Button = styled.button`
    min-width: 40%;
    border: none;
    padding: 15px 20px;
    background-color: pink;
    margin: 10px 5px 5px;
`;


const Register = () => {

    return (
        
        <Container>
            <Wrapper>
                <Title>CREATE AN ACCOUNT</Title>
                <Form >
                    <Input placeholder='First name' />
                    <Input placeholder='Last name' />
                    <Input placeholder='Email' />
                    <Input placeholder='password' />
                    <Input placeholder='confirm password' />
                        <Button>CREATE</Button>        
                </Form>
            </Wrapper>
        </Container>
    )
}

export default Register