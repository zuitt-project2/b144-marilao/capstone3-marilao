import styled from "styled-components"
import AppNavbar from "../components/AppNavbar";
import Announcements from '../components/Announcements'
import Products from '../components/Products'
import Footer from '../components/Footer'
import { mobile } from "../responsive";

const Container = styled.div`
    background-color: #F4C1FA;
`;
const Title = styled.h1`
    margin: 20px;
    text-align: center;
`;
const FilterContainer = styled.div`
    display: flex;
    justify-content: space-between;
`;
const Filter = styled.div`
    margin: 20px;
    ${mobile({width: '0px 20px', display:'flex', flexDirection: 'column' })}
`;
const FilterText = styled.span`
    font-size: 20px;
    font-weight: 600;
    margin-right: 20px;
    ${mobile({ marginRight: '0px' })}
`;

const Select = styled.select`
    padding: 10px;
    margin-right: 20px;
    ${mobile({ margin: '10px 0px' })}
`;
const Option = styled.option`
    
`;
const ProductList = () => {
    return (
        <Container>
            < AppNavbar />
            <Announcements />
            <Title>All Products</Title>
            <FilterContainer>
                <Filter>
                    <FilterText>Categories:</FilterText>
                    <Select>
                        <Option selected>Cosmetics</Option>
                        <Option>Skin Care Products</Option>
                        <Option>Hair Products</Option>
                    </Select>
                </Filter>
            </FilterContainer>
            <Products />
            <Footer />
        </Container>
    )
}

export default ProductList