
import AppNavbar from '../components/AppNavbar';
import Announcements from '../components/Announcements';
import Slider from '../components/Slider';
import Categories from '../components/Categories';
import Products from '../components/Products';
import Footer from '../components/Footer';


const Home = () => {
    return (
        <div>
            <AppNavbar />
            <Announcements />
            <Slider />
            <Categories />
            <Products />
            <Footer />
        </div>
    )
}

export default Home;
