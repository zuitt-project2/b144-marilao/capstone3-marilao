import { Add, Remove } from "@material-ui/icons";
import styled from "styled-components"
import Announcements from "../components/Announcements";
import AppNavbar from "../components/AppNavbar";
import Footer from "../components/Footer";
import { mobile } from "../responsive";

const Container = styled.div`
    background-color: #F4C1FA;
`;
const Title = styled.h1`
    font-weight: 300;
    text-align: center;
`;
const Wrapper = styled.div`
    padding: 20px;
    ${mobile({ padding: '10px' })}
`;
const Top = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 20px;
`;
const Bottom = styled.div`
    display: flex;
    justify-content: space-between;
    ${mobile({ flexDirection: 'column' })}
`;
const Info = styled.div`
    flex: 3;
`;
const Product = styled.div`
    display: flex;
    justify-content: space-between;
    ${mobile({ flexDirection: 'column' })}
`;
const ProductDetail = styled.div`
    flex: 3;
    
`;
const Image = styled.img`
    flex: 3;
    width: 200px;
`;
const Details = styled.div`
    display: flex;
    padding: 20px;
    flex-direction: column;
    justify-content:space-around;
`;
const ProductName = styled.span`
    flex: 3;
`;
const ProductId = styled.span`
    flex: 3;
`;
const ProductColor = styled.div`
    width: 20px;
    height: 20px;
    border-radius: 50%;
    background-color: ${(props) => props.color};
    
`;
const PriceDetail = styled.div`
    flex: 1;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
`;

const TopButton = styled.button`
    padding: 10px;
    font-weight: 600;
    cursor: pointer;
    border: ${props => props.type === 'filled' && 'none'};
    background-color: ${props => props.type === 'filled' ? 'black' : 'transparent'};
    color: ${props => props.type === 'filled' && 'white'};
`;
const TopTexts = styled.div`
${mobile({ display: 'none' })}
`;
const ProductAmountContainer = styled.div`
    display: flex;
    align-items: center;
    margin-bottom: 20px;
    
`;
const ProductAmount = styled.div`
    font-size: 24px;
    display: flex;
    align-items: center;
    justify-content: center;
    width: 30px;
    height: 30px;
    border-radius: 10px;
    border: 1px solid black;
    margin: 0px 5px;
    ${mobile({ margin: '5px 15px' })}
`;
const ProductPrice = styled.div`
    font-size: 30px;
    font-weight: 200;
    ${mobile({ marginBottom: '20px' })}
`;

const TopText = styled.span`
    text-decoration: underline;
    cursor: pointer;
    margin: 0px 10px;
`;

const Hr = styled.hr`
    background-color: black;
    border: 2px solid black;

`;

const Summary = styled.div`
    flex: 1;
    border: 0.5px solid gray;
    border-radius: 20px;
    padding: 20px;
    height: 50vh;
    background-color: white;
`;
const SummaryTitle = styled.h1`
    font-weight: 200;

`;
const SummaryItem = styled.div`
   margin: 30px 0px;
   display: flex;
   font-weight: ${props => props.type === 'total' && '500'};
   font-size: ${props => props.type === 'total' && '24px;'};
   justify-content: space-between;

`;
const SummaryItemText = styled.span`

`;
const SummaryItemPrice = styled.span`
   
`;
const Button = styled.button`
    width: 100%;
    padding: 10px;
    background-color: pink;
    font-weight: 600;
`;
const Cart = () => {
    return (
        <Container>
            <AppNavbar />
            <Announcements />
            <Wrapper>
                <Title>YOUR CART</Title>
                <Top>
                    <TopButton>CONTINUE SHOPPING</TopButton>
                    <TopTexts>
                        <TopText>Shopping Bag(2)</TopText>
                    </TopTexts>
                    <TopButton type='filled'>CHECKOUT NOW</TopButton>
                </Top>
                <Bottom>
                    <Info>
                        <Product>
                            <ProductDetail>
                                <Image src='../images/ceravemoisture.jpg' />
                                <Details>
                                    <ProductName><b>Product:</b>Cerave Moisturizing Cream</ProductName>
                                    <ProductId><b>ID:</b>425694</ProductId>
                                    <ProductColor color='' />
                                </Details>
                            </ProductDetail>
                            <PriceDetail>
                                <ProductAmountContainer>
                                    <Add />
                                    <ProductAmount>1</ProductAmount>
                                    <Remove />
                                </ProductAmountContainer>
                                <ProductPrice>₱ 300</ProductPrice>
                            </PriceDetail>
                        </Product>
                        <Hr />
                        <Product>
                            <ProductDetail>
                                <Image src='../images/hairtreat.jpg' />
                                <Details>
                                    <ProductName><b>Product:</b>Spray</ProductName>
                                    <ProductId><b>ID:</b>423478</ProductId>
                                    <ProductColor color='' />
                                </Details>
                            </ProductDetail>
                            <PriceDetail>
                                <ProductAmountContainer>
                                    <Add />
                                    <ProductAmount>2</ProductAmount>
                                    <Remove />
                                </ProductAmountContainer>
                                <ProductPrice>₱ 500</ProductPrice>
                            </PriceDetail>
                        </Product>
                    </Info>
                    <Summary>
                        <SummaryTitle>Order Summary</SummaryTitle>
                        <SummaryItem>
                            <SummaryItemText>Subtotal</SummaryItemText>
                            <SummaryItemPrice>₱ 1,300</SummaryItemPrice>
                        </SummaryItem>
                        <SummaryItem>
                            <SummaryItemText>Estimated Shipping</SummaryItemText>
                            <SummaryItemPrice>₱ 100</SummaryItemPrice>
                        </SummaryItem>
                        <Hr />
                        <SummaryItem type='total'>
                            <SummaryItemText >Total</SummaryItemText>
                            <SummaryItemPrice>₱ 1,400</SummaryItemPrice>
                        </SummaryItem>
                        <Button>CHECKOUT</Button>
                    </Summary>
                </Bottom>
            </Wrapper>
            <Footer />
        </Container>
    )
}

export default Cart
