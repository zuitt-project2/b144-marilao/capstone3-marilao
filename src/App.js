
import Home from './pages/Home';
import ProductPage from './pages/ProductPage';
import ProductList from './pages/ProductList';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Cart from './pages/Cart';


function App() {

  return <Cart />
}

export default App;
