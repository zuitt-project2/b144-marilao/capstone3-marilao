import { Facebook, Instagram, MailOutline, Phone, Room, YouTube } from '@material-ui/icons';
import React from 'react'
import styled from 'styled-components'
import { mobile } from '../responsive';

const Container = styled.div`
    display: flex;   
    margin-top: 5rem;
    background-color: black;
    ${mobile({ flexDirection: 'column' })}
`;
const Left = styled.div`
    flex: 1;
    display: flex; 
    flex-direction: column;
    padding:20px
    font-color: white;
`;

const Logo = styled.h1`
    color: white;
`;
const Desc = styled.p`
    color: white;
`;
const SocialContainer = styled.div`
    display: flex;
`;
const SocialIcon = styled.div`
    width: 40px;
    height: 40px;
    border-radius: 50%;
    color: white;
    background-color: #${(props) => props.color};
    display: flex;
    align-items: center;
    justify-content: center;
    margin-right: 20px;
`;

const Center = styled.div`
    flex: 1;
    padding: 20px;
    ${mobile({ display: 'none' })}
`;
const Right = styled.div`
    flex: 1;
    padding: 20px;
    ${mobile({ backgroundColor: '#fff8f8' })}
`;

const ContactItem = styled.div`
    color: white;
    margin-bottom: 20px;
    display: flex;
    align-items: center;
`;
const Title = styled.h3`
    color: white;
    margin-bottom: 30px;
`;
const List = styled.ul`
    color: white;
    margin: 0;
    padding: 0;
    list-style: none;
    display: flex;
    flex-wrap: wrap
`;
const ListItem = styled.li`
    width: 50%;
    margin-bottom: 10px;
`;

const Hr = styled.hr`
    
`;
const Footer = () => {
    return (
        <Container>
            <Left>
                <Logo>ShanShop</Logo>
                <Desc>ShanShop offers you quality beauty products that you will love from high-quality cosmetics to skin care products.
                </Desc>
                <SocialContainer>
                    <SocialIcon color='55ACEE'>
                        <Facebook />
                    </SocialIcon>
                    <SocialIcon color='E60023'>
                        <Instagram />
                    </SocialIcon>
                    <SocialIcon color='44BCEF'>
                        <YouTube />
                    </SocialIcon>
                </SocialContainer>
                <Hr />
                <Desc> 2022 || All Rights Reserved.
                </Desc>
            </Left>
            <Center>

                <Title>Quick Links</Title>
                <List>
                    <ListItem>Home</ListItem>
                    <ListItem>All Accessories</ListItem>
                    <ListItem>Login</ListItem>
                    <ListItem>Register</ListItem>
                    <ListItem>Search</ListItem>

                </List>
            </Center>
            <Right>
                <Title>Contact</Title>
                <ContactItem>
                    <Room style={{ marginRight: '10px' }} /> #1st. Phase 2, Taguig, Philippines 1637
                </ContactItem>
                <ContactItem>
                    <Phone style={{ marginRight: '10px' }} /> +63 906 416 0465
                </ContactItem>
                <ContactItem>
                    <MailOutline style={{ marginRight: '10px' }} /> shielarosemarilao@gmail.com
                </ContactItem>
            </Right>
        </Container>
    )
}

export default Footer

