import { Badge } from '@material-ui/core';
import { Search, ShoppingCartOutlined } from '@material-ui/icons';
// import Badge from '@material-ui/core/Badge';
import React from 'react';
import styled from 'styled-components';
import { mobile } from '../responsive';


const Container = styled.div`

    height: 60px;
    ${mobile({ height: '50px' })}
`;

const Wrapper = styled.div`
    display: flex;
    padding: 10px 20px;
    justify-content: space-between;
    align-items: center;
    ${mobile({ padding: '10px 0px' })}
`;
const Left = styled.div`
    flex:1;
    align-items: center;
    display: flex;
`;

const SearchContainer = styled.div`
    border: 0.5px solid lightgray;
    display: flex;
    align-items: center;
    margin-left: 25px;
    padding: 5px;
`
const Input = styled.input`
    border: none;
    ${mobile({ width: '50px' })}
`

const Center = styled.div`
    flex:1;
    text-align: center;
`;

const ImgContainer = styled.div`
    flex:1;
`;
const Logo = styled.div`
    color: red;
    display: flex;
    font-weight: 800;
    font-size: 30px;
    align-items: center;
    justify-content: center;
`;
const Right = styled.div`
    flex:1;
    display: flex;
    align-items: center;
    justify-content: flex-end;
    ${mobile({ flex: 2, justifyContent: 'center' })}
`;

const MenuItem = styled.div`
    font-size: 20px;
    cursor: pointer;
    margin-left: 25px;
    ${mobile({ fontSize: '12px', marginLeft: '10px' })}
`
const AppNavbar = () => {
    return (
        <Container>
            <Wrapper>
                <Left>
                    <SearchContainer>
                        <Input placeholder='Search' />
                        <Search style={{ color: 'gray', fontSize: 20 }} />
                    </SearchContainer>
                </Left>
                <Center>
                    <ImgContainer>
                        <Logo>ShanShop</Logo>
                    </ImgContainer>
                </Center>
                <Right>
                    <MenuItem>HOME
                    </MenuItem>
                    <MenuItem>REGISTER</MenuItem>
                    <MenuItem>SIGN IN</MenuItem>
                    <MenuItem>
                        <Badge badgeContent={4} color="primary">
                            <ShoppingCartOutlined />
                        </Badge>
                    </MenuItem>
                </Right>
            </Wrapper>
        </Container>
    )
}

export default AppNavbar
