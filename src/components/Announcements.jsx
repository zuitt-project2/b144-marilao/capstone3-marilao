import styled from "styled-components"

const Container = styled.div`
    height: 30px;
    background-color: black;
    color: white;
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 15px;
    font-weight: bold;
`

const Announcements = () => {
    return (
        <div>
            <Container>
                Free Shipping on all orders over  ₱ 500.00
            </Container>
        </div>
    )
}

export default Announcements
