import styled from 'styled-components';
import { featuredProducts } from '../data';
import Product from './Product';

const Container = styled.div`
    padding: 20px;
    display: flex;
    flex-wrap: wrap;
    justtify-content: space-between;
`


const Products = () => {
    return (
        <Container>
            {featuredProducts.map((item) => (
                <Product item={item} key={item.id} />
            ))}
        </Container>
    )
}

export default Products

